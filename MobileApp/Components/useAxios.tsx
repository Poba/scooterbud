import { useState, useEffect } from 'react';
import axios from 'axios';

const useAxios = (url: any) => {
    const [data, setData] = useState(null);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const abortCont = new AbortController();
            axios.get(url, { signal: abortCont.signal })
                .then(res => {
                    if (res.status !=200) { // error coming back from server
                        throw Error('could not fetch the data for that resource');
                    }
                    return JSON.parse(JSON.stringify(res)).data;
                })
                .then(data => {
                    setIsPending(false);
                    setData(data);
                    setError(null);
                })
                .catch(err => {
                    if (err.name === 'AbortError') {
                        console.log('fetch aborted')
                    } else {
                        // auto catches network / connection error
                        setIsPending(false);
                        setError(err.message);
                    }
                })

        // abort the fetch
        return () => abortCont.abort();
    }, [url])

    return { data, isPending, error };
}

export default useAxios;