import { createStackNavigator } from '@react-navigation/stack';
import GreetingScreen from './Screens/GreetingScreen';
import SignIn from './Screens/SignIn';
import SignUp from './Screens/SignUp';

const RootStack = createStackNavigator();

//Default screens a signed out user sees.
const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator>
        <RootStack.Screen name="HomeScreen" options={{ headerShown: false }} component={GreetingScreen} />
        <RootStack.Screen name="SignIn" options={{ headerShown: false }} component={SignIn} />
        <RootStack.Screen name="SignUp" options={{ headerShown: false }} component={SignUp} />
    </RootStack.Navigator>
);

export default RootStackScreen;