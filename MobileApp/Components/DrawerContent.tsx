import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { AuthContext } from './context';
import AsyncStorage from '@react-native-async-storage/async-storage';

//Component for filling in the content for the drawer.
export function DrawerContent(props) {

    const [userEmail, setUserEmail] = useState("");
    const [name, setName] = useState('');
    
    const getMail = async () => {
        try {
            let email = await AsyncStorage.getItem('userEmail');
            setUserEmail(email)
            setName(userEmail.slice(0, userEmail.indexOf("@")));
        } catch (error) { }
    }
    useEffect(() => {
        getMail();
    }, []);

    //Using context to get the signOut function.
    const { signOut } = React.useContext(AuthContext);

    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            <Avatar.Image
                                source={{
                                    uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
                                }}
                                size={50}
                            />
                            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                                <Title style={styles.title}>{name}</Title>
                                <Caption style={styles.caption}>{userEmail}</Caption>
                            </View>
                        </View>
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialCommunityIcons
                                    name="account"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Profile"
                            onPress={() => { props.navigation.navigate('UserStatus') }}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section>
                <DrawerItem
                    icon={({ color, size }) => (
                        <MaterialCommunityIcons
                            name="information"
                            color={color}
                            size={size}
                        />
                    )}
                    label="About"
                    onPress={() => { props.navigation.navigate('About') }}
                />
                <DrawerItem
                    icon={({ color, size }) => (
                        <MaterialCommunityIcons
                            name="face-agent"
                            color={color}
                            size={size}
                        />
                    )}
                    label="Contact us"
                    onPress={() => { props.navigation.navigate('Contact') }}
                />
            </Drawer.Section>
            <DrawerItem
                icon={({ color, size }) => (
                    <MaterialCommunityIcons
                        name="exit-to-app"
                        color={color}
                        size={size}
                    />
                )}
                label="Sign Out"
                onPress={() => { signOut() }}
            />
        </View>
    );
}
const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});