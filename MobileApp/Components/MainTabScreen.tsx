import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import Icon from "react-native-paper/lib/typescript/components/Icon";

const MainTab = createMaterialBottomTabNavigator();
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MapScreen from './Screens/MapScreen';
import UserStatus from './Screens/UserStatusScreen';
import UserStackScreen from "./UserStackScreen";

//Main menu a signed in user sees at the bottom of his screen.
function MainTabScreen() {
    return (
        <MainTab.Navigator
            initialRouteName="Map"
            activeColor="#fff"
            inactiveColor="gray"
            barStyle={{ backgroundColor: '#354c85' }}
            shifting
        >
            <MainTab.Screen
                name="Map"
                component={MapScreen}
                options={{
                    tabBarLabel: 'Map',
                    tabBarColor: "#042f66",
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="scooter-electric" color={color} size={26} />
                    ),
                }}
            />
            <MainTab.Screen
                name="User"
                component={UserStackScreen}
                options={{
                    tabBarLabel: 'User',
                    tabBarColor: "#C2d6f5",
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="account" color={color} size={26} />
                    ),
                }}
            />
        </MainTab.Navigator>
    );
}

export default MainTabScreen;