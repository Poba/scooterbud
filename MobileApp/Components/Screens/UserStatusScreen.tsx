import React, { useEffect } from 'react';
import { Text, View, ScrollView, RefreshControl, StyleSheet, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from '../Style';
import { useState } from 'react';
import { AuthContext } from "../context";
import CircularProgress from 'react-native-circular-progress-indicator';
import ScooterBatteryIcon from 'react-native-vector-icons/MaterialIcons';


const UserStatusStyles = styles.UserStatusStyles;

//Screen displaying the current status of the users scooter rent, With Buttons to either return or rent.!
//Preform is logged in check to display proper button.
export default function UserStatusScreen({ navigation }) {

  const { getIzposoja } = React.useContext(AuthContext);

  const [izposoja, setIzposoja] = useState(null);
  const [rentDate, setRentDate] = useState(null);
  const [rentTime, setRentTime] = useState(0);
  /*   const rentDate = ;
*/  /* console.log(rentDate.toUTCString()); */
  // const [date, setDate] = useState(null);
  // const [datePlus1, setDatePlus1] = useState(null);
  // const [currentDate, setCurrentDate] = useState(null);
  useFocusEffect(
    React.useCallback(() => {
      let interval = null;
      getIzposoja()
        .then(data => {
          if (data === undefined) {
            // clearInterval(interval);
            return null;
          } else {
            // interval = setInterval(() => {
            //   console.log(rentTime)
            //   setRentTime((rentTime) => rentTime + 1);
            // }, 1000);
            return JSON.parse(data);
          }
        })
        .then(value => {
          setIzposoja(value);
          setRentDate((new Date(izposoja.time)).toUTCString()); //Converting date to UTC
          console.log(rentDate)
          const dateOfRent = new Date(rentDate);
          console.log(dateOfRent)
          const currentDate = new Date();
          console.log(currentDate)
          const diffTime = Math.abs(currentDate.valueOf() - dateOfRent.valueOf());
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          const diffMinutes = Math.ceil(diffTime / (1000 * 60));
          console.log(diffTime);
          console.log(diffMinutes);
          console.log(diffDays);
        })
        .catch(err => console.log(err));
    }, [])
  );

  /* useEffect(() => {
    let interval = null;
    if (izposoja != null) {
      interval = setInterval(() => {
        if (rentTime === 60.00) {
          Alert.alert("Notice", "60 minutes has elapsed. You will now be charged 2€ for each additional hour!", [
            { text: "Understood" }
          ]);
        }
        console.log(rentTime)
        setRentTime((rentTime) => rentTime + 1);
      }, 1000);
 
      const dateOfRent = new Date(rentDate);
      console.log(dateOfRent)
      const currentDate = new Date();
      console.log(currentDate)
      const diffTime = Math.abs(currentDate.valueOf() - dateOfRent.valueOf());
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      const diffMinutes = Math.ceil(diffTime / (1000 * 60));
      console.log(diffTime);
      console.log(diffMinutes);
      console.log(diffDays);
    }
    else {
      clearInterval(interval);
 
    }
  }, []); */

  // const [scooterId, setScooterId] = useState<string>('');
  // navigation.navigate('QRScreen', {scooter_id: scooter_id});

  // React.useEffect(() => {
  //   let current = new Date();
  //   current.setHours(current.getHours() + 2);
  //   setCurrentDate(current);
  //   let date1 = new Date(date).setHours(new Date(date).getHours() + 1);
  //   setDatePlus1(date1);
  // }, [date]);

  return (
    <View style={UserStatusStyles.container}>
      {/* {error && <Text>{error}</Text>}
      {isPending && <Text>Loading...</Text>} */}
      {/* <ScrollView contentContainerStyle={newStyles.scrollView} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}> */}
      <View style={UserStatusStyles.header}>
        {(izposoja !== null) ? (
          // <TouchableOpacity onPress={() => props.navigation.navigate("QRScreen")}>
          <TouchableOpacity onPress={() => navigation.navigate('QRScreen')}>
            <Animatable.Image
              animation="bounceIn"
              duration={1500}
              source={require("../../assets/Rented.png")}
              style={UserStatusStyles.logo}
              resizeMode="stretch"
            />
          </TouchableOpacity>
        )
          :
          <TouchableOpacity onPress={() => navigation.navigate("QRScreen")}>
            <Animatable.Image
              animation="bounceIn"
              duration={1500}
              source={require("../../assets/Rent.png")}
              style={UserStatusStyles.logo}
              resizeMode="stretch"
            />
          </TouchableOpacity>
        }
      </View>
      <Animatable.View
        style={UserStatusStyles.footer}
        animation="fadeInUpBig"
      >
        <Text style={UserStatusStyles.title}>Rent status</Text>
        <View style={UserStatusStyles.breakRow} />
        <View style={UserStatusStyles.timeRemaining}>
          {(izposoja !== null) ?
            <View style={UserStatusStyles.scooterIconAndId}>
              <ScooterBatteryIcon name='electric-scooter' size={70} />
            </View>
            : null}
          {(izposoja !== null) ?
            <CircularProgress
              value={rentTime}
              maxValue={60}
              radius={80}
              duration={60000}
              initialValue={10}
              activeStrokeColor={'#203e8a'}
              activeStrokeSecondaryColor={'#C25AFF'}
              activeStrokeWidth={20}
              inActiveStrokeWidth={18}
              title={'Time elapsed'}
              titleFontSize={16}
              titleColor={'black'}
              titleStyle={{ fontWeight: 'bold' }}
              /*               progressFormatter={(value: number) => {
                              'worklet';
                              return (value.toFixed(2)); // 2 decimal places
                            }} */

              onAnimationComplete={() => alert("Notice! 60 minutes has elapsed. You will now be charged 2€ for each additional hour!")}
            />
            : null}
          {(izposoja !== null) ?
            <View style={UserStatusStyles.scooterIconAndId}>
              <Text style={UserStatusStyles.numberHeader}>
                #{izposoja.scooter.value.stevilka}
              </Text>
            </View>
            : null}
        </View>

        <View style={UserStatusStyles.breakRow} />
        {(izposoja !== null) ?
          <View style={UserStatusStyles.scooterInfo}>
            <Text style={UserStatusStyles.subtitle}>Time and date of rent:</Text>
            <Text style={UserStatusStyles.rentedDate}> Rented: {rentDate}</Text>
            <View style={UserStatusStyles.breakRow} />
          </View>

          : <Text style={UserStatusStyles.subtitle}>Currently no active rents...</Text>
        }
      </Animatable.View>
      {/* </ScrollView> */}
    </View >
  );
}