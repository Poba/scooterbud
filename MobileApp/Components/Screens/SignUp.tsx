import axios from "axios";
import { useContext, useState } from "react";
import { Text, View, TextInput, TouchableOpacity, StatusBar, Alert } from 'react-native';

import * as Animatable from 'react-native-animatable';
import { LinearGradient } from 'expo-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import styles from '../Style';
import { AuthContext } from "../context";
const signInStyles = styles.signInStyles;

//Components that are within the navigator stack automatically have the navigation prop to use.
function SignUp({ navigation }) {
    const [data, setData] = useState({
        email: "",
        password: "",
        confirmPassword: "",
        checkTextInputChange: false, //For the check icon next to the email.
        secureTextEntry: true, //For the password privacy toggle.
        confirmSecureTextEntry: true,
        isValidEmail: true,
        isValidPassword: true,
        passwordMatches: true,
    })


    //------------------Email and Password validation---------------------------------------
    const textInputChange = (val) => {
        if (val.includes("@")) {
            setData({
                ...data, //Getting existing state and assigning changes
                email: val,
                checkTextInputChange: true,
                isValidEmail: true

            })
        }
        else {
            setData({
                ...data,
                email: val,
                checkTextInputChange: false,
                isValidEmail: false

            })
        }
    }
    const handlePasswordChange = (val) => {
        if (val.trim().length >= 6) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            })
        }
        else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            })
        }
    }
    const handleConfirmPasswordChange = (val) => {
        if (data.password == val) {
            setData({
                ...data,
                confirmPassword: val,
                passwordMatches: true
            })
        }
        else {
            setData({
                ...data,
                confirmPassword: val,
                passwordMatches: false
            })
        }

    }

    //------------------Secure password visibility---------------------------------------
    const updateSecureText = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }
    const updateConfirmSecureText = () => {
        setData({
            ...data,
            confirmSecureTextEntry: !data.confirmSecureTextEntry
        })
    }

    const { signUp } = useContext(AuthContext);

    const pressHandler = (email: string, password: string) => {
        if (data.passwordMatches && data.isValidEmail && data.isValidPassword) {
            signUp(email, password);
            Alert.alert("Account successfully created!", "Please log in.", [
                { text: "Okay" }
            ]);
            navigation.navigate('SignIn');
        }
        else if (data.passwordMatches == false) {
            Alert.alert("Passwords do not match!", "Make sure the passwords match", [
                { text: "Okay" }
            ]);
        }
        else {
            Alert.alert("Invalid email or password!", "Make sure to enter a valid email and password.", [
                { text: "Okay" }
            ]);
        }
    }

    return (
        <View style={signInStyles.container}>
            <StatusBar backgroundColor="#354c85" barStyle="light-content"></StatusBar>
            <View style={signInStyles.header}>
                <Text style={signInStyles.text_header}>Sign Up</Text>
            </View>
            <Animatable.View style={signInStyles.footer} animation="fadeInUpBig">
                <Text style={signInStyles.text_footer}>Email</Text>
                <View style={signInStyles.action}>
                    <FontAwesome
                        name="user-o"
                        color="#05475a"
                        size={20}
                    />
                    <TextInput
                        placeholder="Your Email"
                        style={signInStyles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(val)}
                    />
                    {data.checkTextInputChange ?
                        <Animatable.View
                            animation="bounceIn">
                            <Feather
                                name="check-circle"
                                color="green"
                                size={20}
                            />
                        </Animatable.View>
                        : null}
                </View>
                {data.isValidEmail ? null :
                    <Animatable.Text
                        style={signInStyles.errorMsg}
                        animation="fadeInLeft"
                        duration={500}
                    >Email must be a valid address. </Animatable.Text>
                }
                <Text style={[signInStyles.text_footer, {
                    marginTop: 35
                }]}>Password </Text>
                <View style={signInStyles.action}>
                    <Feather
                        name="lock"
                        color="#05475a"
                        size={20}
                    />
                    <TextInput
                        placeholder="Your Password"
                        style={signInStyles.textInput}
                        autoCapitalize="none"
                        secureTextEntry={data.secureTextEntry ? true : false}
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity
                        onPress={updateSecureText}
                    >
                        {data.secureTextEntry ?
                            <Feather
                                name="eye-off"
                                color="grey"
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color="grey"
                                size={20}
                            />}
                    </TouchableOpacity>
                </View>
                {data.isValidPassword ? null :
                    <Animatable.Text
                        style={signInStyles.errorMsg}
                        animation="fadeInLeft"
                        duration={500}
                    >Password must be at least 6 characters long.</Animatable.Text>
                }
                <Text style={[signInStyles.text_footer, {
                    marginTop: 35
                }]}>Confirm Password </Text>
                <View style={signInStyles.action}>
                    <Feather
                        name="repeat"
                        color="#05475a"
                        size={20}
                    />
                    <TextInput
                        placeholder="Your Password"
                        style={signInStyles.textInput}
                        autoCapitalize="none"
                        secureTextEntry={data.confirmSecureTextEntry ? true : false}
                        onChangeText={(val) => handleConfirmPasswordChange(val)}
                    />
                    <TouchableOpacity
                        onPress={updateConfirmSecureText}
                    >
                        {data.confirmSecureTextEntry ?
                            <Feather
                                name="eye-off"
                                color="grey"
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color="grey"
                                size={20}
                            />}
                    </TouchableOpacity>
                </View>
                {data.passwordMatches ? null :
                    <Animatable.Text
                        style={signInStyles.errorMsg}
                        animation="fadeInLeft"
                        duration={500}
                    >Password does not match.</Animatable.Text>
                }
                <TouchableOpacity onPress={() => pressHandler(data.email, data.password)}>
                    <View style={signInStyles.button}>
                        <LinearGradient
                            colors={['#1B3474', '#2A5CA5']}
                            style={signInStyles.signIn}
                        >
                            <Text style={[signInStyles.textSign, {
                                color: "#fff"
                            }]}>Sign Up</Text>
                        </LinearGradient>
                    </View>
                </TouchableOpacity>
            </Animatable.View>
        </View>
    );
}

export default SignUp;