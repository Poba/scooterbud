import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, Dimensions, Platform } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Camera, CameraType } from 'expo-camera';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { AuthContext } from "../context";
import axios from 'axios';


function QRScreen({ navigation }) {

  const { getIzposoja, getUserToken, deleteIzposoja, saveIzposoja } = React.useContext(AuthContext);

  const [camera, setCamera] = useState(null);
  const [imagePadding, setImagePadding] = useState(0);
  const [ratio, setRatio] = useState('4:3');  // default is 4:3
  const { height, width } = Dimensions.get('window');
  const screenRatio = height / width;
  const [isRatioSet, setIsRatioSet] =  useState(false);
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(CameraType.back);
  const [scanned, setScanned] = useState(false);
  const [izposoja, setIzposoja] = useState(null);
  const [postajaId, setPostajaId] = useState(null);
  const [remount, setRemount] = useState(0);

  useFocusEffect(
    React.useCallback(() => {
      setRemount(remount + 1)
    }, [])
  );

  useEffect(() => {
    console.log('inside qr');
    getIzposoja()
    .then(data => {
      if (data === undefined) {
        return null;
      } else {
        return JSON.parse(data);
      }
    })
    .then(value => {
      if (value !== null)  {
        setIzposoja(value);
        console.log(izposoja);
      }
    })
    .catch(err => console.log(err));
  }, [remount]);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();

    if (hasPermission === null) {
      console.log('haspermisison')
    }
  
    if (hasPermission === false) {
      console.log('haspermisison')
    }

  }, [remount]);


  const prepareRatio = async () => {
    let desiredRatio = '4:3';
    if (Platform.OS === 'android') {
      const ratios = await camera.getSupportedRatiosAsync();
      let distances = {};
      let realRatios = {};
      let minDistance = null;
      for (const ratio of ratios) {
        const parts = ratio.split(':');
        const realRatio = parseInt(parts[0]) / parseInt(parts[1]);
        realRatios[ratio] = realRatio;
        const distance = screenRatio - realRatio; 
        distances[ratio] = realRatio;
        if (minDistance == null) {
          minDistance = ratio;
        } else {
          if (distance >= 0 && distance < distances[minDistance]) {
            minDistance = ratio;
          }
        }
      }
      desiredRatio = minDistance;
      const remainder = Math.floor(
        (height - realRatios[desiredRatio] * width) / 2
      );
      setImagePadding(remainder / 2);
      setRatio(desiredRatio);
      setIsRatioSet(true);
    }
  };

  const setCameraReady = async() => {
    if (!isRatioSet) {
      await prepareRatio();
    }
  };

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    setPostajaId(data);
    console.log('data scanned');
    console.log(data)
  };

  const izposodi = (scooter_id: string) => {
    console.log('inside izposodi');
    console.log(postajaId)
    console.log('scooter_id:')
    console.log(scooter_id);

    let url = 'https://scooterbud3.azurewebsites.net/izposodi';
    let current = new Date();
    // current.setHours(current.getHours() + 2);
    let data = {
        scooter_id: scooter_id,
        user_id: getUserToken(),
        time: current,
    }
    axios.post(url, data)
    .then(response => {
        if (response.status !=200) {
            throw Error('could not fetch the data for that resource');
        }
        return JSON.parse(JSON.stringify(response)).data;
    })
    .then((data) => {
      console.log('save izpoosja');
      console.log(izposoja);
      saveIzposoja(data);
      navigation.navigate('UserStatus');
    })
    .catch((err) => {
        console.log(err);
    });
  }

  const vrni = () => {
    console.log('inside vrni')
    let url = 'https://scooterbud3.azurewebsites.net/vrni/' + izposoja._id + '/' + postajaId;
    axios.get(url)
    .then(response => {
        if (response.status !=200) {
            throw Error('could not fetch the data for that resource');
        }
        return JSON.parse(JSON.stringify(response)).data;
    })
    .then(async (data) => {
      setScanned(false);
      await deleteIzposoja();
      console.log('alles gut');
      Alert.alert(
        '',
        'Scooter was successfully returned.',
        [
          { text: "OK", onPress: () => navigation.navigate('UserStatus') }
        ]
      );
    }) 
    .catch((err) => {
      console.log(err);
    });
  }

  const getPostaja = () => {
    console.log('inaide get postaja')
    console.log(izposoja)
    if (izposoja != null) {
      vrni();
    } else {
      let urlZaPostajo = 'https://scooterbud3.azurewebsites.net/postaja/' + postajaId;
      axios.get(urlZaPostajo)
      .then(response => {
          if (response.status !=200) {
              throw Error('could not fetch the data for that resource');
          }
          return JSON.parse(JSON.stringify(response)).data;
      })
      .then((data) => {
        console.log('inside then')
        console.log(data);
          Alert.alert(
            '',
            `Are you sure do you want to get this scooter? (number: ${data[0].scooter[0].stevilka} )`,
            [
              {
                text: 'No',
                onPress: () => setRemount(remount + 1),
                style: "cancel"
              },
              { text: "Yes", onPress: () => izposodi(data[0].scooter[0]._id) }
            ]
          );
          setScanned(false)
      })
      .catch((err) => {
          console.log(err);
      });
    }
  }

  return(
    <View style={{flex: 1, backgroundColor: 'black'}}>
      <View style={[styles.container, {marginTop: imagePadding, marginBottom: imagePadding}]}>
        <Camera style={styles.camera} type={type} 
          barCodeScannerSettings={{ barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],}} 
          onBarCodeScanned={handleBarCodeScanned}
          onCameraReady={setCameraReady}
          ratio={ratio}
          ref={(ref) => {
            setCamera(ref);
          }}
          >
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setType(
                  // type === Camera.Constants.Type.back
                  //   ? Camera.Constants.Type.front
                  //   : Camera.Constants.Type.back
                  type === CameraType.back
                    ? CameraType.front
                    : CameraType.back
                );
              }}>
              <Text style={styles.text}> Flip </Text>
            </TouchableOpacity>
            {scanned ? getPostaja() : null}
          </View>
        </Camera>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    color: 'black',
    backgroundColor: 'black',
  },
  camera: {
    flex: 1,
    backgroundColor: 'black',
    color: 'black',
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    margin: 20,
  },
  button: {
    flex: 0.1,
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    color: 'white',
    marginBottom: 10,
  },
});

export default QRScreen;
