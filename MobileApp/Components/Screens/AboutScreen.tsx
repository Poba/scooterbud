import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from '../Style';
const HomeScreenStyles = styles.HomeScreenStyles;
import { StyleSheet } from 'react-native';



//First screen a signed out user sees.
export default function AboutScreen({ navigation }) {
    return (
        <View style={HomeScreenStyles.container}>
            <View style={AboutStyles.description}>
                <Text style={AboutStyles.title}>About:</Text>
                <Text style={AboutStyles.subtext}>
                    Scooterbug is a collaborative effort developed by a team of 3 friends with the singular
                    purpose of making clean and sustainable transportation accessible and easy to use for everyone.
                </Text>
            </View>
            <View style={HomeScreenStyles.header}>
                <Animatable.Image
                    animation="bounceIn"
                    duration={1500}
                    source={require("../../assets/Logo.png")}
                    style={HomeScreenStyles.logo}
                    resizeMode="stretch"
                />
            </View>
        </View >
    );
}

const AboutStyles = StyleSheet.create({
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    },
    subtext: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15
    },
    description: {
        marginTop: 50,
        justifyContent: "center",
        alignItems: "center"
    }
});