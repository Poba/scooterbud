import axios from "axios";
import { useContext, useState } from "react";
import { Text, View, TextInput, TouchableOpacity, StatusBar, Alert } from 'react-native';

import * as Animatable from 'react-native-animatable';
import { LinearGradient } from 'expo-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import styles from '../Style';
const signInStyles = styles.signInStyles;
import { AuthContext } from "../context";

//Components that are within the navigator stack automatically have the navigation prop to use.
function SignIn({ navigation }) {

    const [data, setData] = useState({
        email: "",
        password: "",
        checkTextInputChange: false, //For the check icon next to the email.
        secureTextEntry: true, //For the password privacy toggle.
        isValidEmail: true,
        isValidPassword: true
    })


    //------------------Email and Password validation---------------------------------------
    const textInputChange = (val) => {
        if (val.includes("@")) {
            setData({
                ...data, //Getting existing state and assigning changes.
                email: val,
                checkTextInputChange: true,
                isValidEmail: true
            })
        }
        else {
            setData({
                ...data,
                email: val,
                checkTextInputChange: false,
                isValidEmail: false
            })
        }
    }
    const handlePasswordChange = (val) => {
        if (val.trim().length >= 6) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            })
        }
        else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            })
        }

    }
    const updateSecureText = () => { //For the eye icon to change.
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry //Changing the password visibility.
        })
    }

    //Using context to access the signIn function.
    const { signIn } = useContext(AuthContext);

    const loginHandler = async (email, password) => {
        try {
            await signIn(email, password);
        }
        catch (error) {
            Alert.alert("Invalid User!", "Username or password is incorrect. Try again", [
                { text: "Okay" }
            ]);
        }
    }

    return (
        <View style={signInStyles.container}>
            <StatusBar backgroundColor="#354c85" barStyle="light-content"></StatusBar>
            <View style={signInStyles.header}>
                <Text style={signInStyles.text_header}>Sign In</Text>
            </View>
            <Animatable.View style={signInStyles.footer} animation="fadeInUpBig">
                <Text style={signInStyles.text_footer}>Email</Text>
                <View style={signInStyles.action}>
                    <FontAwesome
                        name="user-o"
                        color="#05475a"
                        size={20}
                    />
                    <TextInput
                        placeholder="Your Email"
                        style={signInStyles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(val)}
                    />
                    {data.checkTextInputChange ?
                        <Animatable.View
                            animation="bounceIn">
                            <Feather
                                name="check-circle"
                                color="green"
                                size={20}
                            />
                        </Animatable.View>
                        : null}
                </View>
                {data.isValidEmail ? null :
                    <Animatable.Text
                        style={signInStyles.errorMsg}
                        animation="fadeInLeft"
                        duration={500}
                    >Email must be a valid address. </Animatable.Text>
                }
                <Text style={[signInStyles.text_footer, {
                    marginTop: 35
                }]}>Password </Text>
                <View style={signInStyles.action}>
                    <Feather
                        name="lock"
                        color="#05475a"
                        size={20}
                    />
                    <TextInput
                        placeholder="Your Password"
                        style={signInStyles.textInput}
                        autoCapitalize="none"
                        secureTextEntry={data.secureTextEntry ? true : false}
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity
                        onPress={updateSecureText}
                    >
                        {data.secureTextEntry ?
                            <Feather
                                name="eye-off"
                                color="grey"
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color="grey"
                                size={20}
                            />}
                    </TouchableOpacity>
                </View>
                {data.isValidPassword ? null :
                    <Animatable.Text
                        style={signInStyles.errorMsg}
                        animation="fadeInLeft"
                        duration={500}
                    >Password must be at least 6 characters long.</Animatable.Text>
                }
                <View style={signInStyles.button}>
                    <TouchableOpacity
                        onPress={() => { loginHandler(data.email, data.password) }}
                        style={[signInStyles.signIn, {
                            borderColor: "#354c85",
                            borderWidth: 1,
                            marginTop: 15
                        }]}
                    >
                        <LinearGradient
                            colors={['#1B3474', '#2A5CA5']}
                            style={signInStyles.signIn}
                        >
                            <Text style={[signInStyles.textSign, {
                                color: "#fff"
                            }]}>Sign In</Text>

                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("SignUp")}
                        style={[signInStyles.signIn, {
                            borderColor: "#354c85",
                            borderWidth: 1,
                            marginTop: 15
                        }]}
                    >
                        <Text style={signInStyles.textSign}>Don't have an account? Sign up!</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View>
    );
}

export default SignIn;