import React from 'react';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet, Text, View, Alert, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { AuthContext } from "../context";
import styles from '../Style';
import MapView, { PROVIDER_GOOGLE, Marker, Callout, MapEvent } from 'react-native-maps';
// import MapView, { Marker } from 'react-native-maps';
import useAxios from '../useAxios';
import PostajalisceDetails from './PostajalisceDetails';
import PostajalisceModul from '../../modules/PostajalisceModul';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';

//import moment to help you play with date and time
import moment from 'moment';

const MapScreenStyles = styles.MapScreen;
const initialStyle = styles.initial;



export default function MapScreen({ navigation }) {

  const { getRezervacija, saveRezervacija, deleteRezervacija } = React.useContext(AuthContext);


  const [postajalisceDetailsView, setPostajalisceDetailsView] = React.useState<boolean>(false);
  const [selectedPostajalisce, setSelectedPostajalisce] = React.useState<PostajalisceModul>({id:'', ime: '', lokacija: {coordinates: []}, kapaciteta: 0, postaje: []});
  const [postajalisca, setPostajalisca] = React.useState([]);
  const [remount, setRemount] = React.useState<number>(0);
  const [isLoading, setIsLoading] = React.useState<boolean>(true);
  const [error, setError] = React.useState(null);
  const [reservation, setReservation] = React.useState(null);
  const [showScooters, setShowScooters] = React.useState(true);
  const [colorScooter, setColorScooter] = React.useState('black');
  const [colorParking, setColorParking] = React.useState('rgba(0, 0, 0, 0.3)');
  const [totalDuration, setTotalDuration] = React.useState(0);


React.useEffect(() => {
  const interval = setInterval(() => {
    if (reservation) {
      var date = moment();
      var expirydate = moment(reservation.time).add(15, 'minutes');
      var diffr = moment.duration(moment(expirydate).diff(moment(date)));
      var hours = parseInt(diffr.asHours().toString());
      var minutes = parseInt(diffr.minutes().toString());
      var seconds = parseInt(diffr.seconds().toString());
      var d = hours * 60 * 60 + minutes * 60 + seconds;
      setTotalDuration(d);
    }
  }, 1000);
  return () => {
    clearInterval(interval);
  };
}, [remount, reservation]);

  useFocusEffect(
    React.useCallback(() => {
      console.log('focuess');
      axios.get('https://scooterbud3.azurewebsites.net/postajalisca')
      .then((response) => {
        setPostajalisca(JSON.parse(JSON.stringify(response)).data);
        setIsLoading(false);
        console.log(reservation)
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      });

      getRezervacija()
      .then(data => {
        if (data === undefined) {
          return null;
        } else {
          return JSON.parse(data);
        }
      })
      .then(value => {
        if (value !== null)  {
          setReservation(value);
          console.log(reservation);
        }
      })
      .catch(err => console.log(err));
    }, [])
  );

  React.useEffect(() => {
    if (reservation) {
      saveRezervacija(reservation.time, reservation.num);
    }
  }, [reservation, remount]);


  React.useEffect(() => {
    axios.get('https://scooterbud3.azurewebsites.net/postajalisca')
    .then((response) => {
      setPostajalisca(JSON.parse(JSON.stringify(response)).data);
      setIsLoading(false);
      console.log(reservation)
    })
    .catch((error) => {
      console.log(error);
      setError(error);
    });

    getRezervacija()
    .then(data => {
      if (data === undefined) {
        return null;
      } else {
        return JSON.parse(data);
      }
    })
    .then(value => {
      if (value !== null)  {
        setReservation(value);
        console.log(reservation);
      }
    })
    .catch(err => console.log(err));
  }, [remount]);

  let location = {
    latitude: 46.554650,
    longitude: 15.645881,
    latitudeDelta: 0.009,
    longitudeDelta: 0.009,
  }

  const prenehajRezervacijo = () => {
    let url = 'https://scooterbud3.azurewebsites.net/prenehajrezervacijo/' + reservation.num;
    axios.get(url)
    .then(response => {
        if (response.status !=200) {
            throw Error('could not fetch the data for that resource');
        }
        return JSON.parse(JSON.stringify(response)).data;
    })
    .then((data) => {
        Alert.alert(
            '',
            `Reservation for scooter with number ${reservation.num} was successfully cancelled.`,
            [
                { text: "Ok", onPress: () => {
                  deleteRezervacija();
                  setReservation(null);
                  setRemount(remount + 1);
                }}
            ]
        );
    })
    .catch((err) => {
        console.log(err);
        Alert.alert(
            '',
            `An error has occurred.`,
            [
                { text: "OK", onPress: () => setRemount(remount + 1)}
            ]
        );
    });
  }

  const handleChange = (num) => {
    if (showScooters && num == 2) {
      setShowScooters(false);
      setColorScooter(colorParking);
      setColorParking('black');
    } else if (!showScooters && num == 1) {
      setShowScooters(true);
      setColorScooter(colorParking);
      setColorParking('rgba(0, 0, 0, 0.3)');
    }
  }


  return (
    <View style={initialStyle.container} key={remount}>
      {/* {error && alertFunction(JSON.stringify(error))} */}
      {isLoading && <ActivityIndicator size="large" />}
      {postajalisca!==null &&
        <MapView
          style={initialStyle.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={location}
          mapType='standard'
        >
        {postajalisca.map((postajalisce: PostajalisceModul, i: number) => (
          <Marker
            onPress={() => {
              setPostajalisceDetailsView(true);
              setSelectedPostajalisce(postajalisce);
            }}
            key={i}
            coordinate={{ 
              longitude: postajalisce.lokacija.coordinates[1], 
                latitude: postajalisce.lokacija.coordinates[0] 
            }}
          >
            <Icon name='map-marker' size={60} color='rgba(53, 76, 133, 1)'></Icon>
            <Text style={MarkerStyles.markerText}>{showScooters ? checkScooterAvailability(postajalisce) : postajalisce.kapaciteta - checkScooterAvailability(postajalisce)}</Text>
          </Marker>))
        }
        </MapView>
      }
      {postajalisceDetailsView ? <PostajalisceDetails postajalisce={selectedPostajalisce} availableScooters={checkScooterAvailability(selectedPostajalisce)} onClose={() => setPostajalisceDetailsView(false)} onRemount={() => setRemount(remount+1)} navigation={navigation} onSetReservation={(time, num) => {setReservation({time, num}); setRemount(remount + 1)}} /> : null}
      {(reservation != null) ? <View style={MapScreenStyles.reservation}>
          <Text style={MapScreenStyles.reservationScooterNum}>Reservation #{reservation.num}</Text>
          {console.log('hierr')}
          {console.log(reservation.time)}
          <CountDown
          until={totalDuration}
          /* @ts-ignore */
          timetoShow={['M', 'S']}
          digitStyle={{backgroundColor: '#354c85'}}
          digitTxtStyle={{color: 'white'}}
          // onFinish={() => prenehajRezervacijo()}
          size={15}
        />
          <TouchableOpacity style={MapScreenStyles.reservationButtonView} onPress={() => prenehajRezervacijo()}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Cancel</Text>
            <MaterialIcons name='cancel' size={20} color='white' />
          </TouchableOpacity>
          {/* <TouchableOpacity style={MapScreenStyles.reservationButtonView} onPress={() => {deleteRezervacija(); setRemount(remount + 1)}}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>delete</Text>
            <MaterialIcons name='cancel' size={20} color='white' />
          </TouchableOpacity> */}
        </View> : null}
        <View style={MapScreenStyles.buttonGroup}>
          <TouchableOpacity onPress={() => handleChange(1)}>
            <MaterialCommunityIcons name='scooter' size={40} color={colorScooter} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleChange(2)}>
            <FontAwesome5 name='parking' size={40} color={colorParking} />
          </TouchableOpacity>
        </View>
    </View>
  );
}


const checkScooterAvailability = (postajalisce: PostajalisceModul) => {
  let count = 0;
  postajalisce.postaje.forEach((postaja) => {
    if (postaja.scooter[0] != null && postaja.scooter[0].status === 'ready') {
      count++;
    }
  });
  return count;
}


const MarkerStyles = StyleSheet.create({
  markerText: {
    position: 'absolute', 
    top: 11, 
    alignSelf: 'center', 
    paddingHorizontal: 5, 
    backgroundColor: 'rgba(53, 76, 133, 1)', 
    color: 'white',
  },
  button: {
    backgroundColor: 'blue',
    position: 'absolute',
    bottom: 50,
    padding: 10,
  }
});