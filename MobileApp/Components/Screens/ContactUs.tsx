import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from '../Style';
const HomeScreenStyles = styles.HomeScreenStyles;
import { StyleSheet } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';



//First screen a signed out user sees.
export default function ContactUsScreen({ navigation }) {
    return (
        <View style={HomeScreenStyles.container}>
            <View style={AboutStyles.description}>
                <Text style={AboutStyles.title}>About:</Text>
                <Text style={AboutStyles.subtext}>
                    Email: praktikum043@gmail.com
                </Text>
            </View>
            <View style={HomeScreenStyles.header}>
                <Animatable.View
                    animation="bounceIn"
                    duration={1500}
                    style={AboutStyles.description}
                >
                    <MaterialCommunityIcons
                        name="face-agent"
                        color={"white"}
                        size={200}
                    />

                </Animatable.View>
            </View>
        </View >
    );
}

const AboutStyles = StyleSheet.create({
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    },
    subtext: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15
    },
    description: {
        marginTop: 60,
        justifyContent: "center",
        alignItems: "center"
    }
});