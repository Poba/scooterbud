import React from 'react';
import { Dimensions, StyleSheet, Text, View, TouchableOpacity, FlatList, Alert } from 'react-native';
import PostajalisceModul from '../../modules/PostajalisceModul';
import styles from '../Style';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ScooterBatteryIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import CloseIcon from 'react-native-vector-icons/AntDesign';
import useAxios from '../useAxios';
import ScooterModul from '../../modules/ScooterModul';
import * as Animatable from 'react-native-animatable';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { PanGestureHandler } from 'react-native-gesture-handler';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { LinearGradient } from 'expo-linear-gradient';


const HomeScreenStyles = styles.HomeScreenStyles;
const PostajalisceDetailsStyles = styles.PostajalisceDetailsStyles;

import { AuthContext } from "../context";
import axios from 'axios';


interface PostajalisceDetailsProps {
    onClose: () => any;
    onRemount: () => any;
    onSetReservation: (time, num) => any;
    postajalisce: PostajalisceModul;
    availableScooters: number;
    navigation: any;
}

export default function PostajalisceDetails(props: PostajalisceDetailsProps) {

    
    const rezervirajAlert = async (scooter) => {
        console.log(scooter);
        Alert.alert(
            '',
            `Are you sure you want to reserve this scooter for 15 minutes? (number: ${scooter.stevilka} )`,
            [
              {
                text: 'No',
                onPress: () => console.log('cancel reservation'),
                style: "cancel"
              },
              { text: "Yes", onPress: () => rezerviraj(scooter._id, scooter.stevilka) }
            ]
        );
    }

    const rezerviraj = (id: string, num: number) => {
        let time = new Date().toISOString();
        let url = 'https://scooterbud3.azurewebsites.net/rezerviraj/' + id + '/' + time;
        axios.get(url)
        .then(response => {
            if (response.status !=200) {
                throw Error('could not fetch the data for that resource');
            }
            return JSON.parse(JSON.stringify(response)).data;
        })
        .then((data) => {
            console.log('inside resser')
            console.log(data);
            Alert.alert(
                '',
                `Reservation for scooter with number ${num} was successfully submitted.`,
                [
                    { text: "Yes", onPress: () => {
                        props.onSetReservation(time, num);
                        props.onClose();
                        props.navigation.navigate('Map');
                    }}
                ]
            );
        })
        .catch((err) => {
            console.log(err);
            Alert.alert(
                '',
                `An error has occurred.`,
                [
                    { text: "OK", onPress: () => console.log(props.navigation.navigate('Map'))}
                ]
            );
        });
    }
    

    const handleClose = () => {
        props.onClose();
    }

    return(
    <View style={PostajalisceDetailsStyles.container}>
        <View style={PostajalisceDetailsStyles.header}>
            <Text style={PostajalisceDetailsStyles.naslov}>{props.postajalisce.ime}</Text>
            <View style={PostajalisceDetailsStyles.availabilityView}>
                <View style={PostajalisceDetailsStyles.availabilityView}>
                    <Text style={PostajalisceDetailsStyles.naslov}>{props.availableScooters}</Text>
                    <ScooterBatteryIcon name='electric-scooter' size={50} color='white' />
                </View>
                <View style={PostajalisceDetailsStyles.availabilityView}>
                    <Text style={PostajalisceDetailsStyles.naslov}>{props.postajalisce.kapaciteta - props.availableScooters}</Text>
                    <FontAwesome5 name='parking' size={50} color='white' />
                </View>
            </View>    
        </View>
        
        <Animatable.View
            style={PostajalisceDetailsStyles.footer}
            animation="fadeInUpBig"
            >
            <FlatList style={PostajalisceDetailsStyles.scooterListView} data={props.postajalisce.postaje} renderItem={({item}) => (
                item.scooter[0] != null ? 
                <View style={PostajalisceDetailsStyles.scooterListChild}>
                    <View style={PostajalisceDetailsStyles.scooterNumberView}>
                        <MaterialCommunityIcon name='scooter' size={30} />
                        <Text style={PostajalisceDetailsStyles.scooterChildText}>#{item.scooter[0].stevilka}</Text>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center' }}>
                        {/* <MaterialCommunityIcon name={`battery-charging-${Math.round(item.scooter[0].baterija / 10) * 10}`} /> */}
                        <FontAwesome5 size={30} name={getBatteryName(item.scooter[0].baterija)} />
                        <Text style={{fontWeight: 'bold', marginLeft: 5}}>{item.scooter[0].baterija}%</Text>
                    </View>
                    <TouchableOpacity onPress={() => rezervirajAlert(item.scooter[0])}>
                    <LinearGradient
                        colors={['#1B3474', '#2A5CA5']}
                        style={PostajalisceDetailsStyles.bookNowButton}
                        >
                        <Text style={PostajalisceDetailsStyles.bookNowButtonText} >Book now</Text>
                    </LinearGradient>
                    </TouchableOpacity>
                </View>
                : null )}>
            </FlatList>
        </Animatable.View>
        
        <TouchableOpacity style={PostajalisceDetailsStyles.closeButton} onPress={handleClose} >
            <CloseIcon color='white' name='close' size={30} />
        </TouchableOpacity>
    </View>
    
    );
}

function getBatteryName(baterija) {
    if (baterija < 25) {
        return 'battery-empty';
    } else if (baterija >= 25 && baterija < 50) {
        return 'battery-quarter';
    } else if (baterija >= 50 && baterija < 75) {
        return 'battery-half';
    } else if (baterija >= 75 && baterija < 100) {
        return 'battery-three-quarters';
    } else if (baterija == 100) {
        return 'battery-full';
    }
}


const detailsStyles = StyleSheet.create({
    view: {
        width: Dimensions.get('window').width,
        height: '60%',
        backgroundColor: 'white',
        padding: 15,
    },
    headerView: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 10,
    },
    availabilityView: {
        // flex: 0.2,
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    scooterListView: {
        marginTop: 10,
        borderTopColor: 'black',
        borderTopWidth: 1,
        flex: 1,
        flexDirection: 'column',
    },
    scooterListChild: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        elevation: 1,
        padding: '5%',
        alignItems: 'center',
        // backgroundColor: 'green',
        // marginHorizontal: 10,
        marginVertical: 5,
        color: 'black,'
    },
    closeButton: {
        position: 'absolute',
        right: 20,
        top: 20,
        padding: 5,
    },
    izposojaButton: {
        backgroundColor: 'red',
        color: 'white',
        padding: '2%',
    },
    naslov: {
        padding: 5,
        fontSize: 20,
        fontWeight: 'bold',
    }
});