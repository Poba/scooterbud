import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from '../Style';
const HomeScreenStyles = styles.HomeScreenStyles;


//First screen a signed out user sees.
export default function GreetingScreen({ navigation }) {
  return (
    <View style={HomeScreenStyles.container}>
      <View style={HomeScreenStyles.header}>
        <Animatable.Image
          animation="bounceIn"
          duration={1500}
          source={require("../../assets/Logo.png")}
          style={HomeScreenStyles.logo}
          resizeMode="stretch"
        />
      </View>
      <Animatable.View
        style={HomeScreenStyles.footer}
        animation="fadeInUpBig"
      >
        <Text style={HomeScreenStyles.title}>Get started!</Text>
        <Text style={HomeScreenStyles.text}>Sign in with account</Text>
        <View style={HomeScreenStyles.button}>
          <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
            <LinearGradient
              colors={['#1B3474', '#2A5CA5']}
              style={HomeScreenStyles.signIn}
            >
              <Text style={HomeScreenStyles.textSign}>Get started</Text>
              <MaterialIcons
                name="navigate-next"
                color="#fff"
                size={20}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View >
  );
}