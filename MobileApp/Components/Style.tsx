import { StyleSheet } from 'react-native';
import { Dimensions, Platform } from 'react-native';

const initial = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  input: {
    borderWidth: 1,
    borderColor: '#777',
    borderRadius: 20,
    padding: 8,
    margin: 10,
    width: 200,
  },
  signInButton: {
    marginHorizontal: 10,
    marginTop: 24,
    padding: 30,
    backgroundColor: '#3366FF',
    fontSize: 24,
    borderRadius: 40,
    fontWeight: "bold"
  },
  text: {
    fontWeight: "bold",
    fontSize: 20
  }

});

const { height } = Dimensions.get("screen");
const height_logo = height * 0.30;

const HomeScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#354c85'
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30
  },
  logo: {
    width: height_logo,
    height: height_logo
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
  },
  text: {
    color: 'grey',
    marginTop: 5
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold'
  }
});

const signInStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#354c85'
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  }
});


const PostajalisceDetailsStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: Dimensions.get('window').width,
    zIndex: 10,
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 2,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingTop: 20,
    paddingHorizontal: 30
  },
  availabilityView: {
    // flex: 0.2,
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
},
scooterListView: {
    flex: 1,
    flexDirection: 'column',
},
scooterListChild: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '5%',
    alignItems: 'center',
    marginVertical: 5,
    color: 'black',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
},
scooterNumberView: {
  // flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
},
scooterChildText: {
  fontWeight: 'bold',
  fontSize: 30,
  marginLeft: 5,
},
closeButton: {
    position: 'absolute',
    right: 20,
    top: 65,
    padding: 5,
    zIndex: 10,
    elevation: 10,
    text: 'black',
},
izposojaButton: {
    backgroundColor: 'red',
    color: 'white',
    padding: '2%',
},
naslov: {
    padding: 5,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
},
bookNowButtonText: {
  color: 'white',
  padding: 10,
},
bookNowButton: {
  borderRadius: 5,
},
});


const MapScreen = StyleSheet.create({
  reservation: {
    flex: 1,
    position: 'absolute',
    top: 110,
    left: 20,
    borderRadius: 20,
    zIndex: 3,
    elevation: 3,
    backgroundColor: 'white',
    padding: 15,
    alignItems: 'center',
  },
  reservationScooterNum: {
    fontWeight: 'bold',
  },
  reservationButtonView: {
    backgroundColor: 'rgb(209, 46, 46)',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    padding: 10,
    borderRadius: 10,
  },
  buttonGroup: {
    flex: 1,
    justifyContent: 'space-around',
    position: 'absolute',
    bottom: 20,
    left: 20,
    width: '40%',
    borderRadius: 20,
    zIndex: 3,
    elevation: 3,
    backgroundColor: 'white',
    padding: 15,
    alignItems: 'center',
    flexDirection: 'row',
  }
});
// export default { HomeScreenStyles: HomeScreenStyles, signInStyles: signInStyles, initial: initial, PostajalisceDetailsStyles: PostajalisceDetailsStyles, MapScreen: MapScreen };
    
const UserStatusStyles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#354c85',
    alignItems: 'center'
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    flexWrap: "wrap",
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  scooterInfo: {
    flexDirection: 'row',
    marginBottom: 250,
    flexWrap: "wrap",
    justifyContent: "center"
  },
  rentedDate: {
    fontWeight: 'bold',
    fontSize: 18,
    color: "#203e8a"
  },
  timeRemaining: {
    paddingTop: 8,
    flexDirection: "row"
  },
  scooterIconAndId: {
    paddingTop: 50,
    marginRight: 10,
    marginLeft: 10
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  subtitle: {
    color: "#000000",
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10
  },
  breakRow: {
    flexBasis: "100%",
    height: 0,
  },
  logo: {
    width: height_logo,
    height: height_logo
  },

  numberHeader: {
    fontWeight: 'bold',
    fontSize: 44,
  },
});

export default {
  HomeScreenStyles: HomeScreenStyles,
  signInStyles: signInStyles,
  initial: initial,
  UserStatusStyles: UserStatusStyles,
  PostajalisceDetailsStyles: PostajalisceDetailsStyles, 
  MapScreen: MapScreen
};
