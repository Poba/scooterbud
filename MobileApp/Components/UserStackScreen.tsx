import { createStackNavigator } from '@react-navigation/stack';
import UserStatusScreen from './Screens/UserStatusScreen';
import QRScreen from './Screens/QRScreen';

const UserStack = createStackNavigator();

//Screen for the UserStatus screen so the user can just click on a button to switch to scaning
const UserStackScreen = () => {
    return (
    <UserStack.Navigator>
        <UserStack.Screen name="UserStatus" options={{ headerShown: false }} component={UserStatusScreen} />
        <UserStack.Screen name="QRScreen" options={{ headerShown: false }} component={QRScreen} />
    </UserStack.Navigator>
    );
};

export default UserStackScreen;