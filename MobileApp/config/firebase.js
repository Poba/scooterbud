import { initializeApp } from "firebase/app";


// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDX9XhiF6MNBJTonRLaldsUoZ9boMc_x7k",
    authDomain: "scooterbud-d9320.firebaseapp.com",
    projectId: "scooterbud-d9320",
    storageBucket: "scooterbud-d9320.appspot.com",
    messagingSenderId: "81510143025",
    appId: "1:81510143025:web:5f776336b035e0bac37110"
};

const Firebase = initializeApp(firebaseConfig);

export default Firebase;