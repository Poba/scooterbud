import React, { useEffect, useMemo } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { AuthContext } from './Components/context';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { ActivityIndicator, View } from 'react-native';

import RootStackScreen from './Components/RootStackScreen';
import MainTabScreen from './Components/MainTabScreen';
import QRScreen from './Components/Screens/QRScreen';
import { DrawerContent } from './Components/DrawerContent';
import UserStatusScreen from './Components/Screens/UserStatusScreen';

import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import Firebase from './config/firebase';

import IzposojaModul from './modules/IzposojaModul';
import AboutScreen from './Components/Screens/AboutScreen';
import ContactUsScreen from './Components/Screens/ContactUs';


const auth = getAuth(Firebase);

const Drawer = createDrawerNavigator();

export default function App() {
  //Initial state for the loading status and the email an token of the user
  const initialLoginState = {
    isLoading: false,
    email: null,
    userToken: null,
  }

  //All method's for changing state centralized within this Reducer function. Makes things easier.
  //Takes in action object and state of the data we are manipulating.
  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case "RETRIEVE_TOKEN":
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false
        };
      case "LOGIN":
        return {
          ...prevState,
          email: action.id,
          userToken: action.token,
          isLoading: false
        };
      case "LOGOUT":
        return {
          ...prevState,
          email: null,
          userToken: null,
          isLoading: false
        };
      case "REGISTER":
        return {
          ...prevState,
          email: action.id,
          userToken: action.token,
          isLoading: false
        };
    }
  }

  //The action object has a type attribute to describes the type of change we want to make to the state
  //and a optional payload.
  //The useReducer function takes in the reducer to use and the initial state we defined.
  //Dispatch function sends the desired action to reducer. All in one function.
  //Reducer returns new state in the form of loginState.
  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);

  //Caching the auth function for better optimization. Could take second argument in array
  //so it knows when to run the function and when to keep the old state.
  //Run code when you have to and not when you don't actually need to.
  const authContext = useMemo(() => ({
    signIn: async (email: string, password: string) => {
      let userToken = null;
      let userEmail = null;
      //Make DB Check
      await signInWithEmailAndPassword(auth, email, password).then(async (userCredential) => {
        const user = userCredential.user;
        userToken = user.uid;
        userEmail = user.email;
        await AsyncStorage.setItem('userToken', userToken) //Setting user token in storage.
        await AsyncStorage.setItem('userEmail', userEmail) //Setting user token in storage.
        console.log(AsyncStorage.getItem('userToken'));
      }).catch((error) => {
        console.log(error.message);
        throw error;
      });
      dispatch({ type: "LOGIN", id: email, token: userToken })
    },
    signOut: async () => {
      try {
        await auth.signOut();
        await AsyncStorage.removeItem('userToken');
        await AsyncStorage.removeItem('userEmail');
        await AsyncStorage.removeItem('izposoja');
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: "LOGOUT" })
    },
    signUp: async (email: string, password: string) => {
      await createUserWithEmailAndPassword(auth, email, password).then((userCredential) => {
        const user = userCredential.user;
      }).catch((error) => {
        console.log(error);
      });
    },
    getUserToken: () => {
      return auth.currentUser.uid;
    },
    saveIzposoja: async (izposoja) => {
      await AsyncStorage.setItem('izposoja', JSON.stringify(izposoja));
      return true;
    },
    getIzposoja: async () => {
      try {
        const data = await AsyncStorage.getItem('izposoja');
        if (data !== null) {
          console.log('inside getIzposoja');
          console.log(data);
          return data;
        }
      } catch (error) {
        console.log(error);
        return error;
      }
    },
    deleteIzposoja: async () => {
      console.log('inside delete izposoja');
      await AsyncStorage.removeItem('izposoja');
      return true;
    },
    saveRezervacija: async (time, num) => {
      await AsyncStorage.setItem('rezervacija', JSON.stringify({time: time, num: num}));
      return true;
    },
    getRezervacija: async () => {
      try {
        const data = await AsyncStorage.getItem('rezervacija');
        if (data !== null) {
          console.log('inside getRezervacija');
          console.log(data);
          return data;
        }
      } catch (error) {
        console.log(error);
        return error;
      }
    },
    deleteRezervacija: async () => {
      console.log('inside delete rezervacija');
      await AsyncStorage.removeItem('rezervacija');
      return true;
    },
  }), []);

  //Runs when the screen is rendering. In this time we check if the user is logged in.
  useEffect(() => {
    setTimeout(async () => {
      let userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: "RETRIEVE_TOKEN", token: userToken })
    }, 1000)
  }, [])


  //Loading screen.
  if (loginState.isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
  //If the userToken is not null it will present the tab navigation with the other screens. 
  //If it is null the user is presented with the default signIn/up screens.
  //Provider passes context to nested components to use.
  //We pass in the drawer navigator the content we want to display in the drawer as well 
  //as the tab screen navigator.
  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {loginState.userToken != null ? (
          <Drawer.Navigator initialRouteName='HomeDrawer' drawerContent={props => <DrawerContent {...props} />}>
            <Drawer.Screen name="HomeDrawer" options={{ headerTitle: "", headerTransparent: true }} component={MainTabScreen} />
            <Drawer.Screen name="UserStatus" options={{ headerTitle: "", headerTransparent: true }} component={UserStatusScreen} />
            <Drawer.Screen name="About" options={{ headerTitle: "", headerTransparent: true, }} component={AboutScreen} />
            <Drawer.Screen name="Contact" options={{ headerTitle: "", headerTransparent: true, }} component={ContactUsScreen} />
          </Drawer.Navigator>
        )
          :
          <RootStackScreen navigation={undefined} />
        }
      </NavigationContainer>
    </AuthContext.Provider >
  );
}


