import PostajaModul from "./PostajaModul";

export default interface PostajalisceModul {
    id: string;
    ime: string;
    lokacija: {
        coordinates: number[];
    };
    kapaciteta: number;
    postaje: PostajaModul [];
}