import ScooterModul from './ScooterModul';

export default interface PostajaModul {
    id: string;
    available: boolean;
    scooter: ScooterModul;
}