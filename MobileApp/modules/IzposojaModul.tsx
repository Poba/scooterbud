import ScooterModul from "./ScooterModul";

export default interface IzposojaModul {
    _id: string;
    user_id: string;
    scooter_id: string;
    time: string;
    active: boolean;
    scooter: ScooterModul;
}