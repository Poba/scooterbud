export default interface ScooterModul {
    id: string;
    status: string;
    stevilka: number;
    baterija: number;
}