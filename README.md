# ScooterBud
<p align="center">
<img  width="200" height="auto" src="./MobileApp/assets/Logo.png" />
</p>

## Description
ScooterBud is an Android app for renting electric scooters. The main goal is to make an application that is able to log rents and states of scooters. It allows users to register an account and and then be able to rent or return a scooter to or from a station by scanning a QR code. It also gives users the ability to reserve i.e. book a scooter for 15 minutes.

## Download link(apk)
- Download and install apk on your android device. <a href="https://gitlab.com/Poba/scooterbud/-/raw/main/ScooterBud.apk " download>Click to Download</a>

## Admin page
- https://scooterbud3.azurewebsites.net/
- Password: admin

## Screenshots

<p float="left">
  <img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/start.png" />
  <img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/mapa.png" />
  <img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/postajalisce.png" />
  <img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/rezervacija.png" />
   <img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/rent.png" />
<img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/renting.png" />
<img  width="300" height="auto" src="./_PROMOCIJA/Screenshoti/rented.png" />

</p>




## Tech stack
<p align="center">
<img  width="400" height="auto" src="./MobileApp/assets/TechStack.png" />
</p>
<!-- ### Mobile application
- React Native
- TypeScript
### Backend and admin page
- Javascript 
- FireBase (Authentication)
- HTML
- CSS
- NodeJS
- Express
- MongoDB -->


## For development
### Requirements
- installed NodeJS (16.x)
- installed expo cli
```
npm install -g expo-cli
```

### Database installation
1. Download and install MongoDB community edition(5.x)
```
https://www.mongodb.com/try/download/community
```
2. Navigate to folder 'db' and run below command to seed the database
```
node db_init.js
```

### Backend(Express server)
1. Build Docker image from DockerFile in 'server' folder
2. Run docker image

### Mobile app
First navigate to MobileApp folder then run these commands
1. npm install
2. expo start
Then it will automatically open a window in your browser.
3. Install Expo Go on your Android device
4. Make sure both devices are on the same network
5. Scan Qr code in your browser window with Expo Go application
6. If there are connection problems, try switching to tunnel mode in the browser.

#### Test user for MobileApp
Email: jaka.jevsnik@mail.si
Password: geslo123

## Roles
- User (authenticated through Firebase)
- Admin (authenticated through server)


