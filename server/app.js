var express = require('express');
var app = express();
var path = require('path');
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

app.use(express.static(path.join(__dirname, 'public')));

// View engine setup
// app.set('views', path.join(__dirname, 'public'));
// app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


//----------------------- Route setup ----------------------------------------------
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');



app.use('/', indexRouter);
app.use('/users', usersRouter);


//----------------------- Error handeling ----------------------------------------------
// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


//PORT
// app.set('port', process.env.PORT || 443);
// // const port = process.env.PORT || 3000;
// app.listen(app.get('port'), () => console.log(`Listening on port ${port}...`));

module.exports = app;
