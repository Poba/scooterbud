/* const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/"; */
const ObjectId = require('mongodb').ObjectId;
const { MongoClient, url } = require("./dbConnection");

let postajalisca = [
    {
        ime: 'FERI, Koroška 46',
        lokacija: {
            type: "Point",
            coordinates: [46.558965, 15.637974]
        },
        kapaciteta: 15,
        postaje: [],
    },
    {
        ime: 'Stara trta, Vojašniška ulica',
        lokacija: {
            type: "Point",
            coordinates: [46.556671, 15.644186]
        },
        kapaciteta: 15,
        postaje: [],
    },
    {
        ime: 'Glavni trg 16',
        lokacija: {
            type: "Point",
            coordinates: [46.557556, 15.645892]
        },
        kapaciteta: 20,
        postaje: [],
    },
    {
        ime: 'Upravna enotam, Ulica heroja Staneta',
        lokacija: {
            type: "Point",
            coordinates: [46.562506, 15.649862]
        },
        kapaciteta: 10,
        postaje: [],
    },
    {
        ime: 'Ljudski vrt, Prežihova ulica',
        lokacija: {
            type: "Point",
            coordinates: [46.562281, 15.639133]
        },
        kapaciteta: 7,
        postaje: [],
    },
    {
        ime: 'Ljudski vrt, Mladinska ulica',
        lokacija: {
            type: "Point",
            coordinates: [46.563786, 15.639825]
        },
        kapaciteta: 15,
        postaje: [],
    },
]

let postaja = {
    available: false,
    scooter: '',
}

let scooter = {
    status: 'ready',
    stevilka: -1,
    baterija: 100,
}

function nafilajPostajalisca(dbo, postajalisca, postaje, callback) {
    let postajeIDs = postaje.map((postaja) => { return ObjectId(postaja.toString()); });

    postajalisca.forEach((postajalisce, i) => {
        postajalisce.postaje = postajeIDs.slice(0, postajalisce.kapaciteta);
        postajeIDs.splice(0, postajalisce.kapaciteta);
        // console.log(postajalisce);
        dbo.collection('postajalisce').insertOne(postajalisce, (err, res) => {
            if (err)
                throw err;
            console.log(res);
            if (i === postajalisca.length-1) {
                callback();
            }
        });
    });
}

function nafilajScooterje(dbo, postajalisca, scooter, callback) {
    let sum = 0;
    postajalisca.forEach((postajalisce) => {
        sum += postajalisce.kapaciteta;
    });
    console.log(sum)

    let scooters = [];
    for (let i = 0; i < sum; i++) {
        let s = JSON.parse(JSON.stringify(scooter));
        s.stevilka = i+1;
        scooters.push(s);
    }
    dbo.collection('scooter').insertMany(scooters, (err, res) => {
        if (err)
            throw err;
        let postaje = [];
        let arr = Object.values(res.insertedIds);
        arr.forEach((scooter) => {
            let p = JSON.parse(JSON.stringify(postaja));
            console.log(scooter.toString())
            p.scooter = ObjectId(scooter.toString());
            postaje.push(p);
        });
        dbo.collection('postaja').insertMany(postaje, (err, res) => {
            if (err)
                throw err;
            console.log(Object.values(res.insertedIds));
            callback(Object.values(res.insertedIds));
        });
    });
}


MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    nafilajScooterje(dbo, postajalisca, scooter, (postaje) => {
        nafilajPostajalisca(dbo, postajalisca, postaje, () => {
            db.close();
        });
    });
});