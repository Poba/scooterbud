const table = document.getElementById('table');

const getIzposoje = async () => {
    const response = await fetch('/izposoje');
    const izposoje = await response.json(); //extract JSON from the http response
    
    console.log(izposoje);
    
    izposoje.forEach(element => {
        table.insertAdjacentHTML(
        'beforeend',
        `<tr class="${element.active}" onClick="${element._id}">
            <td>${element.user_id}</td>
            <td>${element.scooter[0].stevilka}</td>
            <td>${element.time}</td>
            <td>${element.active}</td>
        </tr>`
        );
    });
    }
    
    getIzposoje();

    const checkbox = document.getElementById('aktivne')

    checkbox.addEventListener('change', (event) => {
    if (event.currentTarget.checked) {
        //alert('checked');
        var neaktivne = document.getElementsByClassName(false);
        for(var i = 0; i < neaktivne.length;i++)
            neaktivne[i].style.display = 'none';
        
    } else {
        //alert('not checked');
        var neaktivne = document.getElementsByClassName("false");
        for(var i = 0; i < neaktivne.length;i++)
            neaktivne[i].style.display = '';
    }
    })