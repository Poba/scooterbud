const table = document.getElementById('table');

const getSkuterji = async () => {
    const response = await fetch('/skuterji');
    const skuterji = await response.json(); //extract JSON from the http response
    
    console.log(skuterji);
    
    skuterji.forEach(element => {
        table.insertAdjacentHTML(
        'beforeend',
        `<tr onClick="${element._id}">
            <td>${element.stevilka}</td>
            <td>${element.baterija}</td>
            <td>${element.status}</td>
        </tr>`
        );
    });
    }
    
    getSkuterji();