const table = document.getElementById('table');
const Modal = new bootstrap.Modal(document.getElementById('Modal'), {});

var postajalisca;
const getPostajalisca = async () => {
const response = await fetch('/postajalisca');
postajalisca = await response.json(); //extract JSON from the http response

console.log(postajalisca);

postajalisca.forEach(element => {
    var prosto = 0;
    element.postaje.forEach(postaja => {
        if(postaja.available)
            prosto++;
    });

    table.insertAdjacentHTML(
    'beforeend',
    `<tr onClick="setAndOpen('${element._id}')">
        <td>${element.ime}</td>
        <td>@${element.lokacija.coordinates}</td>
        <td>${element.kapaciteta}</td>
        <td>${prosto}</td>
    </tr>`
    );
    
});
}

getPostajalisca();

function Go(url){
    window.location.href = url;
}

function setAndOpen(id){
    
    /* Get Postaja */
    var toPostajalisce;
    postajalisca.forEach(element => {
        if(element._id === id){
            toPostajalisce = element;
            return;
        }
    });

    // prosta mesta
    var prosto = 0;
    toPostajalisce.postaje.forEach(postaja => {
        if(postaja.available)
            prosto++;
    });

    document.getElementById('ModalTitle').innerHTML = toPostajalisce.ime;
    document.getElementById('Kapaciteta').innerHTML = "Kapaciteta: " + toPostajalisce.kapaciteta;
    document.getElementById('Prosto').innerHTML = "Prosto: " + prosto;
    document.getElementById('Lokacija').innerHTML = "Lokacija: " + toPostajalisce.lokacija.coordinates;

    const postaje = document.getElementById('postaje');
    postaje.replaceChildren();
    toPostajalisce.postaje.forEach((element,index) => {
        postaje.insertAdjacentHTML(
        'beforeend',
        `<tr>
            <td>${index}</td>
            <td>${element.available}</td>
            <td>${(element.scooter[0] != undefined)?element.scooter[0].stevilka:"/"}</td>
            <td>${(element.scooter[0] != undefined)?element.scooter[0].baterija:"/"}</td>
            <td>${(element.scooter[0] != undefined)?element.scooter[0].status:"/"}</td>
        </tr>`
        );
        
    });

    Modal.show()
}