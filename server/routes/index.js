const express = require('express');
const router = express.Router();
const { MongoClient, url } = require("../db/dbConnection");
const ObjectId = require('mongodb').ObjectID;


/* const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
 */

// GET postajalisca
router.get('/postajalisca', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('postajalisce').aggregate(
      [
        {
          $lookup: {
            from: 'postaja',
            localField: 'postaje',
            foreignField: '_id',
            pipeline: [
              {
                $lookup: {
                  from: 'scooter',
                  localField: 'scooter',
                  foreignField: '_id',
                  as: 'scooter',
                }
              }
            ],
            as: 'postaje',
          }
        }
      ]
    ).toArray((err, result) => {
      if (err) throw err;

      // console.log(result)
      res.send(result);
      db.close();
    });
  });
});

router.get('/skuterji', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('scooter').find()
    .toArray((err, result) => {
      if (err) throw err;

      // console.log(result)
      res.send(result);
      db.close();
    });
  });
});

router.get('/izposoje', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('izposoja').aggregate(
      [
        {
          $lookup: {
            from: 'scooter',
            localField: 'scooter_id',
            foreignField: '_id',
            as: 'scooter',
          }
        }
      ]
    ).toArray((err, result) => {
      if (err) throw err;

      // console.log(result)
      res.send(result);
      db.close();
    });
  });
});

router.post('/izposodi', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    console.log(req.body);

    let izposoja = {};

    dbo.collection('izposoja').insertOne({ user_id: req.body.user_id, scooter_id: ObjectId(req.body.scooter_id), time: req.body.time, active: true }, (err, result) => {
      if (err) {
        res.status(500).send(err);
        console.log(err);
      }
      izposoja._id = result.insertedId;
      izposoja.user_id = req.body.user_id;
      izposoja.scooter_id = req.body.scooter_id;
      izposoja.active = true;
      izposoja.time = req.body.time;

      dbo.collection('scooter').findOneAndUpdate({ '_id': ObjectId(req.body.scooter_id) }, { $set: { status: 'izposojen' }}, { upsert: false, returnOriginal: false }, (err, result) => {
        if (err) {
          res.status(500).send(err);
          console.log(err);
        }
        izposoja.scooter = result;
        dbo.collection('postaja').updateOne( {'scooter': ObjectId(req.body.scooter_id) }, { $set: { available: true, scooter: null }}, { upsert: false }, (err, result) => {
          if (err) {
            res.status(500).send(err);
            console.log(err);
          }
          izposoja.scooter.value.status = 'izposojen';
          console.log(izposoja);
          res.status(200).send(izposoja);
          db.close();
        });
      });
    });
  });
});


router.get('/vrni/:izposoja_id/:postaja_id', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('izposoja').findOneAndUpdate({ _id: ObjectId(req.params.izposoja_id)}, { $set: { active: false }}, { upsert: false, returnOriginal: false }, async (err, result) => {
      if (err) {
        res.status(500).send(err);
      }
      let scooter_id = await result.value.scooter_id.toString();
      console.log('scooter_id: ')
      console.log(scooter_id);
      console.log(typeof(scooter_id));
      dbo.collection('postaja').findOneAndUpdate({ _id: ObjectId(req.params.postaja_id) }, { $set: { scooter: ObjectId(scooter_id), available: false }}, { upsert: false, returnOriginal: false }, (err, result) => {
        if (err) {
          res.status(500).send(err);
        }
        console.log(result);
        dbo.collection('scooter').findOneAndUpdate({ _id: ObjectId(scooter_id) }, { $set: { status: 'ready' } }, { upsert: false, returnOriginal: false }, (err, result) => {
          if (err) {
            res.status(500).send(err);
          }
          res.status(200).send(result);
          db.close();
        });
      });
    });
  });
});

router.get('/postaja/:postaja_id', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('postaja').aggregate(
      [
        {$match:
          {'_id': ObjectId(req.params.postaja_id),}
        },
        {
          $lookup: {
            from: 'scooter',
            localField: 'scooter',
            foreignField: '_id',
            as: 'scooter',
          }
        }
      ]
    ).toArray((err, result) => {
      if (err) throw err;

      // console.log(result)
      res.send(result);
      db.close();
    });
  });
});

router.get('/update/:postaja_id', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('postaja').findOneAndUpdate({ _id: ObjectId(req.params.postaja_id) }, { $set: { available: true, scooter: ObjectId('000000000000000000000000')}}, (err, result) => {
      if (err) throw err;

      console.log(result);
      res.send(result);
    });

  });
});


router.get('/scooter/:user_id', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    // dbo.collection('izposoja').findOne({ user_id: req.params.user_id, active: true }, (err, result) => {
    //   if (err) {
    //     console.log(err);
    //     res.status(500).send(err);
    //   }

    //   console.log(result);
    //   res.status(200).send(result);
    //   db.close();
    // });

    dbo.collection('izposoja').aggregate(
      [
        {
          $lookup: {
            from: 'scooter',
            localField: 'scooter_id',
            foreignField: '_id',
            as: 'scooter',
          }
        }
      ]
    ).toArray((err, result) => {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
      console.log(result);
      res.status(200).send(result);
      db.close();
    });
  });
});


router.get('/rezerviraj/:scooter_id/:date', (req, res, next) => {
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    dbo.collection('rezervacija').insertOne({ scooter_id: req.params.scooter_id, time: req.params.date, active: true }, (err, result) => {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
      dbo.collection('scooter').findOneAndUpdate({ _id: ObjectId(req.params.scooter_id) }, { $set: { status: 'unavailable' } }, (err, result) => {
        if (err) {
          res.status(500).send(err);
        }
        res.status(200).send(result);
        db.close();
      });
    });
  });
});

router.get('/prenehajrezervacijo/:scooter_num/', (req, res, next) => {
  console.log(req.params.scooter_num);
  MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db('test_db_4');

    try {
      dbo.collection('scooter').findOneAndUpdate({ stevilka: parseInt(req.params.scooter_num) }, { $set: { status: 'ready' } }, { upsert: false, returnOriginal: false }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(500).send(err);
        }
        console.log(result)
        dbo.collection('rezervacija').findOneAndUpdate({ scooter_id: ObjectId(result.value._id), active: true }, { $set: { active: false } }, (err, result) => {
          if (err) {
            res.status(500).send(err);
          }
          res.status(200).send(result);
          db.close();
        });
      });
    } catch(err) {
      console.log(err);
    }
  });
});


module.exports = router;
